%% Hyperparameters

clear 
close all

% This is the absolute filepath to the primary directory
root_folder = '/home/ryanress/Nextcloud/Graduate/Yazdan Lab/RR_inhibition_analysis';

% This is relative filepath to the dataset you are analyzing
data_folder = '2021-09-01'; 

% Filename of the impedance.txt file
imp_fname = "impedance-9-1-21.txt";

% Filename without extension 
data_fname = "900ms_2s_30p_ch14a-110ma";
stim_in_ns5 = true; % If you have a .ns5 file for stim set this to true

% The channel which contains stim data
%stim_ch = 10245; % For LED data
stim_ch = 10241; % For laser data

% Channels to plot for analysis
ch_to_plot = "14a";     

% Title to add to figures
plot_title = "2021-09-01 Laser 110ma";
%% Plot Impedence & Array

%
% Set channel referencing structures
%

base_path = fullfile(root_folder, data_folder);

num_channels = 32;
ch_idx = 1:num_channels;
ch_names = [""];
for i = 1:16
    ch_names(i) = sprintf("%da", i);
    ch_names(16+i) = sprintf("%db", i);
end
ch_name2ch_idx = containers.Map(ch_names,ch_idx); 

%
% Load impedance data and plot locations and impedances
%

imp_raw=load_impedance(fullfile(base_path, imp_fname));
imp_rows = [1:16, 33:48];
imp = imp_raw(imp_rows);

% X and Y locations for array
resolFactor = 100; % holdover from mapArray.m
ch_x = [4 4 3 2 2 2 1 1 1 1 2 2 3 3 3 4 4 4 5 6 6 7 8 7 7 7 6 6 6 5 5 5]*0.75*resolFactor;
ch_y = [3 1 2 1 5 3 4 2 8 6 9 7 6 4 8 5 7 9 8 9 7 8 5 6 2 4 3 5 1 6 4 2]*0.75*resolFactor;

figure
scatter(ch_x,ch_y, 100, imp, 'filled')
map = [linspace(0,1,200)
       linspace(1,0,200)
       linspace(0,0,200)]';
colormap(map)
colorbar
caxis([min(imp) max(imp)])

for i = ch_idx
    txt = "  "+ch_names(i);
    text(ch_x(i),ch_y(i),txt)
end

title('Impedance Channel Map')

%% Run analysis


ns_ch_offset=32*4;
ns_ch_idx = [1:16, ns_ch_offset+1:ns_ch_offset+16];

full_path = convertStringsToChars(fullfile(base_path,data_fname));

for ch_name = ch_to_plot
    
    %
    % Preprocessing
    %

    plot_ch_idx = ch_name2ch_idx(ch_name);
    plot_ns_ch_idx = ns_ch_idx(plot_ch_idx);
    ch_key = sprintf("CH%d", plot_ns_ch_idx);
    thresh = 20;
    
    % Load NS data:
    % data is a struct with elec (electrophysiology) and stim (light
    % stimulus) fields. loadNSData automatically parses stimulus onsets,
    % offsets, and durations.
    data = loadNSData(full_path, plot_ns_ch_idx, stim_in_ns5, stim_ch, thresh);
    
    % These parameters define the windows used for stimulus triggered
    % averages (in samples)
    win_start = -999;
    win_end = 1000;

    % exclude pulse onset if spacing is less than 200ms (i.e. during a pulse
    % train)
    min_trigger_spacing = 200; 
    triggers_mask = [inf; diff(data.stim.onset)] > min_trigger_spacing;
    triggers = data.stim.onset(triggers_mask);

    % Construct stacked arrays of stimlus triggered ECoG signal windows.
    [t_stacked, y_stacked] = buildTriggeredArray(data.elec.(ch_key), ...
                    triggers, ...
                    win_start, win_end, data.elec.fs);
    
    % Calculate STA
    y_sta = mean(y_stacked, 2);
    
    % Construct stacked array of stimulus signals. This is used to generate
    % a visual indication of when stimulus is active.
    [t_stacked, stim_stacked] = buildTriggeredArray(data.stim.y, ...
                    triggers, ...
                    win_start, win_end, data.elec.fs);
    
    % Find onsets and offsets of stimulus during the STA window
    avg_stim = mean(stim_stacked, 2);
    stim_threshold = avg_stim > thresh;
    stim_onset = find([0; diff(stim_threshold) == 1]);
    stim_offset = find([0; diff(stim_threshold) == -1]);
    stim_periods = [t_stacked(stim_onset)' t_stacked(stim_offset)'];
    
    %
    % Plot STA
    %
    
    figure
    title("STA: "+plot_title + " CH" + ch_name)
    plotSTA(t_stacked, y_stacked, stim_periods, 'r')
    xlabel('time (s)')
    ylabel('uV')
    
    %
    % Plot STA Spectrogram + Z score 
    % Note that these are not demeaned and have significant spectral
    % leakage!
    %
    
    figure
    
    % Spectrogram parameters
    T = .1; % s
    W = 20; % hz
    window_params=[T T/4];
    params.tapers = [T*W round(2*T*W-1)]; % TW, K (num params
    params.Fs = data.elec.fs;
    params.fpass = [1 200];
    [S,t,f] = mtspecgramc(y_sta, window_params, params);
    t_S = t + win_start / data.elec.fs;
    
    subplot(2,1,1)
    plot_matrix(S, t_S, f, 'l');
    title('STA Spectrogram')
    
    % Z scoring (assumes that stimulus starts at t=0 and we are normalizing
    % to the region for t<0)
    S_pre = S(t_S < 0,:);
    S_pre_mu = mean(S_pre, 1);
    S_pre_std = std(S_pre);
    Z = (S - S_pre_mu) ./ S_pre_std;

    subplot(2,1,2)
    plot_matrix(Z, t_S, f, 'n');
    title('STA Z Score')
    caxis([-5 5])
    colormap jet
    
    %
    % Plot Average of stimulus triggered spectrograms
    %
    
    figure
    S_avg = 0;
    for k = 1:size(y_stacked, 2)
        % Resuses parameters from previous spectrograms
        [S,t,f] = mtspecgramc(y_stacked(:,k), window_params, params);
        S_avg = S_avg + S;
    end
    S_avg = S_avg / size(y_stacked, 2);
    t_avg = t + win_start / data.elec.fs;

    subplot(2,1,1)
    plot_matrix(S_avg, t_avg, f, 'l');
    title('Stimulus Triggered Spectrogram Average')
    
    S_avg_pre = S_avg(t_avg < 0,:);
    S_avg_mu = mean(S_avg_pre, 1);
    S_avg_std = std(S_avg_pre);
    Z_avg = (S_avg - S_avg_mu) ./ S_avg_std;
    
    subplot(2,1,2)
    plot_matrix(Z_avg, t_avg, f, 'n');
    title('Averaged Z Score')
    caxis([-5 5])
    colormap jet
    
    %
    % Power t-tests
    %
    
    % Define bands of interest
    bands(1) = struct("high", 30, "low", 12, "name", "Beta");
    bands(2) = struct("high", 60, "low", 30, "name", "Gamma");
    bands(3) = struct("high", 200, "low", 60, "name", "High Gamma");
    
    %
    % Plot stim and no stim signal regions for sanity check
    %
    
    figure
    plot(data.elec.t, data.elec.(ch_key))
    hold on
    
    num_onsets = length(data.stim.onset);
    % This determines the region of time (in seconds) relative to stimulus 
    % turning on that is defined as the "stim" region. 
    onset_range = [.1 .8];
    onset_color = 'cyan';
    
    num_offsets = length(data.stim.offset);
    % This determines the region of time (in seconds) relative to stimulus 
    % turning off that is defined as the "no stim" region. 
    offset_range = [.1 .9];
    offset_color = 'red';
    
    y_min = min(data.elec.(ch_key));
    y_max = max(data.elec.(ch_key));
    % Plot Stim Regions
    for i = 1:num_onsets
        idx_onset = data.stim.onset(i);
        t_start = data.elec.t(idx_onset) + onset_range(1);
        t_end = data.elec.t(idx_onset) + onset_range(2);
        x_stim = [t_start, t_end, t_end, t_start];
        y_stim = [y_min, y_min, y_max, y_max];
        patch(x_stim, y_stim, onset_color, 'FaceAlpha', .3, 'LineStyle', 'none');
    end

    % Plot No Stim Regions
	for i = 1:num_offsets
        idx_offset = data.stim.offset(i);
        t_start = data.elec.t(idx_offset) + offset_range(1);
        t_end = data.elec.t(idx_offset) + offset_range(2);
        x_stim = [t_start, t_end, t_end, t_start];
        y_stim = [y_min, y_min, y_max, y_max];
        patch(x_stim, y_stim, offset_color, 'FaceAlpha', .3, 'LineStyle', 'none');
    end
    title("Stim and No Stim Regions: Channel "+ch_name)
    
    
    %
    % Plot power t-tests for each band 
    %
    
    figure
    num_bands = length(bands);
    for i = 1:num_bands
        band = bands(i);
        sig = data.elec.(ch_key);
                            
        onset_powers = zeros(num_onsets,1);
        for j = 1:num_onsets
            % Define region after stimulus onset
            idx_onset = data.stim.onset(j);
            t_start = data.elec.t(idx_onset) + onset_range(1);
            t_end = data.elec.t(idx_onset) + onset_range(2);
            % Crop signal region
            cropped_sig = sig(t_start < data.elec.t & data.elec.t < t_end);
            % Bandpass filter to band-of-interest
            filt_sig = BPfilter(cropped_sig,...
                                data.elec.fs, band.low, band.high);
            % Calculate power over cropped signal region
            onset_powers(j) = SignalPower(filt_sig, data.elec.fs);
        end
        
        offset_powers = zeros(num_offsets,1);
        for j = 1:num_offsets
            % Define region after stimulus offset
            idx_offset = data.stim.offset(j);
            t_start = data.elec.t(idx_offset) + offset_range(1);
            t_end = data.elec.t(idx_offset) + offset_range(2);
            % Crop signal region
            cropped_sig = sig(t_start < data.elec.t & data.elec.t < t_end);
            % Bandpass filter to band-of-interest
            filt_sig = BPfilter(cropped_sig,...
                                data.elec.fs, band.low, band.high);
            % Calculate power over cropped signal region
            offset_powers(j) = SignalPower(filt_sig, data.elec.fs);
        end
        
        % plot a boxplot with overlayed scatter plot
        subplot(1, num_bands,i)
        hold on
        h_plot = boxplot([offset_powers, onset_powers], {'No Stim', 'Stim'});
        
        scatter(ones(num_offsets, 1)*1, offset_powers);
        scatter(ones(num_offsets, 1)*2, onset_powers);

        [h,p] = ttest(offset_powers, onset_powers);
        title("Channel "+ch_name+ " - " + band.name)
        subtitle(sprintf("p value: %f",p))
    end
    
    
    %
    % Plot averaged periodograms to double check results of t-test
    %
    
    figure
    
    % Periodogram parameters
    noverlap = 25;
    window_length = 300; % hamming window
    nfft = window_length;
    
    % Loop over stimulus regions and average periodograms
    pxx_len = floor(nfft / 2 + 1);
    pxx_stim = zeros(pxx_len,1);
    for i = 1:num_onsets
        idx_onset = data.stim.onset(i);
        t_start = data.elec.t(idx_onset) + onset_range(1);
        t_end = data.elec.t(idx_onset) + onset_range(2);
        t = data.elec.t;
        y_stim = data.elec.(ch_key)(t_start <= t & t < t_end);
        [pxx,f_pre] = pwelch(y_stim,window_length,noverlap,nfft,data.elec.fs);
        pxx_stim = pxx_stim + pxx / num_onsets;
    end
    plot(f_pre, 10*log10(pxx_stim), 'r','DisplayName','Stim')
    hold on

    % Loop over no stim regions and average periodograms
    pxx_no_stim = zeros(pxx_len, 1);
	for i = 1:num_offsets
        idx_offset = data.stim.offset(i);
        t_start = data.elec.t(idx_offset) + offset_range(1);
        t_end = data.elec.t(idx_offset) + offset_range(2);
        t = data.elec.t;
        y_no_stim = data.elec.(ch_key)(t_start <= t & t < t_end);
        [pxx,f_post] = pwelch(y_no_stim,window_length,noverlap,nfft,data.elec.fs);
        pxx_no_stim = pxx_no_stim + pxx / num_onsets;
    end
    plot(f_post, 10*log10(pxx_no_stim), 'k','DisplayName','No Stim')
    
    title("Averaged Periodograms")
    xlabel('f (Hz)')
    ylabel('dB')
    legend
    
end

function plotSTA(t, stacked, stim_periods, LineSpec)
    %
    % This function plots an STA given a stacked array of stimulus
    % triggered window of dimensions (num_triggers, num_samples). It also
    % accepts a (num_pulses, 2) array of stim_periods which defines which
    % areas should be filled in to show when stimulus is on.
    %
    grey_trace_color=[.5,.5,.5, .3];
    y = mean(stacked,2);
    hold on
    xlim([min(t), max(t)]);
    plot(t, stacked, 'color', grey_trace_color);
    
    [m, min_idx] = min(y);
    min_std = std(stacked(min_idx,:));
    [m, max_idx] = max(y);
    max_std = std(stacked(max_idx,:));
    y_min = min(y) - min_std*1.5;
    y_max = max(y) + max_std*1.5; 
    ylim([y_min, y_max])
    
    for i = 1:size(stim_periods, 1)
       x_min = stim_periods(i,1);
       x_max = stim_periods(i,2);
       x_stim = [x_min, x_max, x_max, x_min];
       y_stim = [y_min, y_min, y_max, y_max];
       patch(x_stim, y_stim, 'cyan', 'FaceAlpha', .3, 'LineStyle', 'none');
    end
    
    plot(t, y, LineSpec, 'LineWidth', 2);
    
end
            
function [t, stacked] = buildTriggeredArray(signal, triggers, window_start, window_end, fs)
    %
    % This function takes signal (num_samples, 1) and a triggers 
    % (num_triggers, 1) arrays and constructs a stacked array of windows
    % surrounding each trigger. 
    %
    num_triggers = length(triggers);
    window_width = window_end - window_start+1;
    stacked = zeros(window_width, num_triggers);
    for i = 1:num_triggers
       start_idx = triggers(i) + window_start;
       stop_idx = triggers(i) + window_end;
       stacked(:, i) = signal(start_idx:stop_idx);
    end
    t = (window_start:window_end) / fs;
end

function out = loadNSData(file_stub, channels, stim_30k, stim_ch, stim_thresh)
    %
    % This function loads .ns2 and .ns5 data files into a single data
    % structure. 
    %
    % Parameters:
    % file_stub - name of .ns2/5 files to be loaded
    % channels - array of channel ids to be loaded into the array
    % stim_30k - if set to true then stimulus data is loaded from .ns5 file
    % stim_ch - channel index where stimulus data is stored
    % stim_thresh - threshold used for finding when the stimulus is on
    %
    % Returns:
    % out - structure with the following fields
    % out.elec.file - file used to load electrophysiology data
    % out.elec.fs - sampling rate of electrophysiology data
    % out.elec.t - array of time points for each sample
    % out.elec.CH[XX] - electrophysiology data recorded on channel [XX]
    % out.stim.file - file used to load stimulus data
    % out.stim.fs - sampling rate of stimulus data
    % out.stim.t - array of time points for each sample of stimulus data
    % out.stim.y - stimulus data
    % out.stim.onset - array of the when stimulus turns on in samples
    % out.stim.offset - array of the when stimulus turns off in samples 
    % out.stim.duration - array of the duration of each stimulus pulse in
    %                     samples
    out.elec.file = [file_stub '.ns2'];
    out.elec.fs = 1000;
    % Load electrode data
    for ch = channels
        [x,y]=extract_all_data_function('LFP',ch,false,out.elec.file);
        out.elec.t = x;
        out.elec.(['CH' int2str(ch)]) = y';
    end
    
    % Load stimulus data
    if stim_30k
        out.stim.file = [file_stub '.ns5'];
        [x_stim,y_stim]=extract_all_data_function('Analog 30k', stim_ch,false,out.stim.file);
        out.stim.t = downsample(x_stim, 30);
        out.stim.y = downsample(y_stim, 30)';
    else
        out.stim.file = [file_stub '.ns2'];
        [x_stim,y_stim]=extract_all_data_function('Analog 1k', ...
                                              stim_ch,false,out.stim.file);                                    
        out.stim.t = x_stim;
        out.stim.y = y_stim';
          
    end
    % Parse stim onset, offset, and durations
    out.stim.fs = 1000;
    
    stim_threshold = out.stim.y > stim_thresh;
    stim_onset = [0; diff(stim_threshold) == 1];
    stim_offset = [0; diff(stim_threshold) == -1];
    
    out.stim.onset = find(stim_onset);
    out.stim.offset = find(stim_offset);   
    
    % fix starting / stopping in middle of pulse
    if out.stim.offset(1) < out.stim.onset(1)
        out.stim.offset = out.stim.offset(2:end);
    end
    
    if out.stim.offset(end) < out.stim.onset(end)
        out.stim.onset = out.stim.onset(1:end-1);
    end
    out.stim.duration = out.stim.offset - out.stim.onset;
end