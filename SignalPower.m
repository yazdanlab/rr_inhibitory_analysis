function Power=SignalPower(Signal,SamplingFreq)
% this function calcualtes the signal power 

time=length(Signal)/SamplingFreq;
Power=sum(Signal.^2)/time;

end