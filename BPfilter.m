function Output=BPfilter(Input, SamplingFreq, Low, High)

%% Filter specifications
% filter params
n = 2; Wn1 = [Low High]/(SamplingFreq/2);
ftype = 'bandpass';
% Transfer Function design
[b1,a1] = butter(n,Wn1,ftype);
%% Filter the signal
Output=filtfilt(b1,a1,double(Input));



end