function imp = load_impedance(fname)

    all_lines = readlines(fname);
    skip_lines = 11;

    imp_lines = all_lines(skip_lines:end);
    num_lines = length(imp_lines);
    num_cols = sum(split(imp_lines(1), ' ') ~= '');

    imp_col = 8;
    imp = zeros(num_lines,1);
    for i = 1:num_lines-1
        line = split(imp_lines(i), ' ');
        line = line(line ~= '');
        imp(i) = double(line(imp_col));
    end
end



