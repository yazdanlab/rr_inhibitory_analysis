%% File Names / Locations

clear 
close all

% data_folder = '2021-09-01';
% imp_fname = "impedance-9-1-21.txt";
% data_fname = "900ms_2s_30p_ch14a-110ma";
% stim_in_ns5 = true;
% %stim_ch = 10245; % For LED data
% stim_ch = 10241; % For laser data
% 
% plot_title = "2021-09-01 Laser 110ma";
% 
% ch_to_plot = ["14a"];%, "5a", "6a", "15b"];% ch_names(imp < 200);%["16a", "12b"];

% 09-08 Saline
data_folder = '2021-09-08';
imp_fname = "impedance-9-8-21.txt";
data_fname = "900ms_1s_30p-700mv";
stim_in_ns5 = true;
stim_ch = 10245; % For LED data
%stim_ch = 10241; % For laser data

ch_to_plot = ["14b"];% ch_names(imp < 200);%["16a", "12b"];

% 02-14 Agar
% data_folder = '2022-02-14-benchside-agar-LED-testing';
% imp_fname = "A5_impedance_2022-02-14_agar.txt";
% data_fname = "A5_RED_agar_3200mV_900ms_30p_1s";
% stim_in_ns5 = false;
% stim_ch = 10245; % For LED data
% %stim_ch = 10241; % For laser data
% 
% ch_to_plot = ["14b"];

% 01-24 Saline
% data_folder = '2022-01-24';
% imp_fname = "A4_impedance_2022-01-24.txt";
% data_fname = "A4_fullLED_red_700mV_1s_900ms_30p_saline";
% stim_in_ns5 = false;
% stim_ch = 10245; % For LED data
% %stim_ch = 10241; % For laser data
% 
% ch_to_plot = ["14b"];
%% Plot Impedence & Array


root_folder = '/home/ryanress/Nextcloud/Graduate/Yazdan Lab/RR_inhibition_analysis';
%data_folder = '2022-02-14-benchside-agar-LED-testing';
base_path = fullfile(root_folder, data_folder);

num_channels = 32;
ch_idx = 1:num_channels;
ch_names = [""];
for i = 1:16
    ch_names(i) = sprintf("%da", i);
    ch_names(16+i) = sprintf("%db", i);
end
ch_name2ch_idx = containers.Map(ch_names,ch_idx); 

%
% Load impedance data 
%

%imp_fname = "A5_impedance_2022-02-14_agar.txt";
imp_raw=load_impedance(fullfile(base_path, imp_fname));
imp_rows = [1:16, 33:48];
imp = imp_raw(imp_rows);

if ch_to_plot == "all"
    ch_to_plot = ch_names(imp < 200);
end

% X and Y locations for array
resolFactor = 100; % holdover from mapArray.m
ch_x = [4 4 3 2 2 2 1 1 1 1 2 2 3 3 3 4 4 4 5 6 6 7 8 7 7 7 6 6 6 5 5 5]*0.75*resolFactor;
ch_y = [3 1 2 1 5 3 4 2 8 6 9 7 6 4 8 5 7 9 8 9 7 8 5 6 2 4 3 5 1 6 4 2]*0.75*resolFactor;

figure
scatter(ch_x,ch_y, 100, imp, 'filled')
map = [linspace(0,1,200)
       linspace(1,0,200)
       linspace(0,0,200)]';
colormap(map)
colorbar
caxis([min(imp) max(imp)])

for i = ch_idx
    txt = "  "+ch_names(i);
    text(ch_x(i),ch_y(i),txt)
end

title('Impedance Channel Map')

%%
%
% Load NS data
%
ns_ch_offset=32*4;
ns_ch_idx = [1:16, ns_ch_offset+1:ns_ch_offset+16];

%fname = "A5_agar_3200mV_900ms_30p_1s";
%stim_in_ns5 = false;

full_path = convertStringsToChars(fullfile(base_path,data_fname));

for ch_name = ch_to_plot
    plot_ch_idx = ch_name2ch_idx(ch_name);
    plot_ns_ch_idx = ns_ch_idx(plot_ch_idx);
    ch_key = sprintf("CH%d", plot_ns_ch_idx);
    %stim_ch = 10245; % Sometimes 10241
    thresh = 20;
    data = loadNSData(full_path, plot_ns_ch_idx, stim_in_ns5, stim_ch, thresh);
    
    y_min = min(data.elec.(ch_key));
    y_max = max(data.elec.(ch_key));
                            
    
    bands(1) = struct("high", 30, "low", 12, "name", "Beta");
    bands(2) = struct("high", 60, "low", 30, "name", "Gamma");
    bands(3) = struct("high", 200, "low", 60, "name", "High Gamma");
    
    % Plot stim and no stim signal regions
    figure
    plot(data.elec.t, data.elec.(ch_key))
    hold on
    
    num_onsets = length(data.stim.onset);
    onset_range = [.1 .8];
    onset_color = 'cyan';
    
    num_offsets = length(data.stim.offset);
    offset_range = [.1 .9];
    offset_color = 'red';
    
    % Plot Stim Regions
    for i = 1:num_onsets
        
        idx_onset = data.stim.onset(i);
        t_start = data.elec.t(idx_onset) + onset_range(1);
        t_end = data.elec.t(idx_onset) + onset_range(2);
        x_stim = [t_start, t_end, t_end, t_start];
        y_stim = [y_min, y_min, y_max, y_max];
        patch(x_stim, y_stim, onset_color, 'FaceAlpha', .3, 'LineStyle', 'none');
    end

    % No Stim Regions
	for i = 1:num_offsets
        idx_offset = data.stim.offset(i);
        t_start = data.elec.t(idx_offset) + offset_range(1);
        t_end = data.elec.t(idx_offset) + offset_range(2);
        x_stim = [t_start, t_end, t_end, t_start];
        y_stim = [y_min, y_min, y_max, y_max];
        patch(x_stim, y_stim, offset_color, 'FaceAlpha', .3, 'LineStyle', 'none');
    end
    title("Stim and No Stim Regions: Channel "+ch_name)
%     legend
    
    figure
    num_bands = length(bands);
    for i = 1:num_bands
        band = bands(i);
        sig = data.elec.(ch_key);
                            
        onset_powers = zeros(num_onsets,1);
        for j = 1:num_onsets
            idx_onset = data.stim.onset(j);
            t_start = data.elec.t(idx_onset) + onset_range(1);
            t_end = data.elec.t(idx_onset) + onset_range(2);
            cropped_sig = sig(t_start < data.elec.t & data.elec.t < t_end);
            filt_sig = BPfilter(cropped_sig,...
                                data.elec.fs, band.low, band.high);
            onset_powers(j) = SignalPower(filt_sig, data.elec.fs);
        end
        
        offset_powers = zeros(num_offsets,1);
        for j = 1:num_offsets
            idx_offset = data.stim.offset(j);
            t_start = data.elec.t(idx_offset) + offset_range(1);
            t_end = data.elec.t(idx_offset) + offset_range(2);
            cropped_sig = sig(t_start < data.elec.t & data.elec.t < t_end);
            filt_sig = BPfilter(cropped_sig,...
                                data.elec.fs, band.low, band.high);
            offset_powers(j) = SignalPower(filt_sig, data.elec.fs);
        end
        
        subplot(1, num_bands,i)
        hold on
        h_plot = boxplot([offset_powers, onset_powers], {'No Stim', 'Stim'});
        
        scatter(ones(num_offsets, 1)*1, offset_powers);
        scatter(ones(num_offsets, 1)*2, onset_powers);

        [h,p] = ttest(offset_powers, onset_powers);
        title("Channel "+ch_name+ " - " + band.name)
        subtitle(sprintf("p value: %f",p))
        
    end
    %suptitle(sprintf("T-Tests: Channel "+ch_name));
    
    % Periodograms
    figure
    noverlap = 25;
    window_length = 300; % hamming window
    nfft = window_length;
    pxx_len = floor(nfft / 2 + 1);
    pxx_stim = zeros(pxx_len,1);
    % Plot Stim Regions
    for i = 1:num_onsets
        idx_onset = data.stim.onset(i);
        t_start = data.elec.t(idx_onset) + onset_range(1);
        t_end = data.elec.t(idx_onset) + onset_range(2);
        t = data.elec.t;
        y_stim = data.elec.(ch_key)(t_start <= t & t < t_end);
        [pxx,f_pre] = pwelch(y_stim,window_length,noverlap,nfft,data.elec.fs);
        pxx_stim = pxx_stim + pxx / num_onsets;
    end
    plot(f_pre, 10*log10(pxx_stim), 'r','DisplayName','Stim')
    hold on

    % No Stim Regions
    pxx_no_stim = zeros(pxx_len, 1);
	for i = 1:num_offsets
        idx_offset = data.stim.offset(i);
        t_start = data.elec.t(idx_offset) + offset_range(1);
        t_end = data.elec.t(idx_offset) + offset_range(2);
        t = data.elec.t;
        y_no_stim = data.elec.(ch_key)(t_start <= t & t < t_end);
        [pxx,f_post] = pwelch(y_no_stim,window_length,noverlap,nfft,data.elec.fs);
        pxx_no_stim = pxx_no_stim + pxx / num_onsets;
    end
    plot(f_post, 10*log10(pxx_no_stim), 'k','DisplayName','No Stim')
    
    title("Averaged Periodograms")
    xlabel('f (Hz)')
    ylabel('dB')
    legend
    
    
    
    
    
  
end

function plotSTA(t, stacked, stim_periods, LineSpec)
    grey_trace_color=[.5,.5,.5, .3];
    y = mean(stacked,2);
    hold on
    xlim([min(t), max(t)]);
    plot(t, stacked, 'color', grey_trace_color);
    
    [m, min_idx] = min(y);
    min_std = std(stacked(min_idx,:));
    [m, max_idx] = max(y);
    max_std = std(stacked(max_idx,:));
    y_min = min(y) - min_std*1.5;
    y_max = max(y) + max_std*1.5; 
    ylim([y_min, y_max])
    
    for i = 1:size(stim_periods, 1)
       x_min = stim_periods(i,1);
       x_max = stim_periods(i,2);
       x_stim = [x_min, x_max, x_max, x_min];
       y_stim = [y_min, y_min, y_max, y_max];
       patch(x_stim, y_stim, 'cyan', 'FaceAlpha', .3, 'LineStyle', 'none');
    end
    
    plot(t, y, LineSpec, 'LineWidth', 2);
    
end
            
function [t, stacked] = buildTriggeredArray(signal, triggers, window_start, window_end, fs)
    num_triggers = length(triggers);
    window_width = window_end - window_start+1;
    stacked = zeros(window_width, num_triggers);
    for i = 1:num_triggers
       start_idx = triggers(i) + window_start;
       stop_idx = triggers(i) + window_end;
       stacked(:, i) = signal(start_idx:stop_idx);
    end
    t = (window_start:window_end) / fs;
end

function out = loadNSData(file_stub, channels, stim_30k, stim_ch, stim_thresh)
    
    out.elec.file = [file_stub '.ns2'];
    out.elec.fs = 1000;
    % Load electrode data
    for ch = channels
        [x,y]=extract_all_data_function('LFP',ch,false,out.elec.file);
        out.elec.t = x;
        out.elec.(['CH' int2str(ch)]) = y';
    end
    
    % Load stimulus data
    if stim_30k
        out.stim.file = [file_stub '.ns5'];
        [x_stim,y_stim]=extract_all_data_function('Analog 30k', stim_ch,false,out.stim.file);
        out.stim.t = downsample(x_stim, 30);
        out.stim.y = downsample(y_stim, 30)';
    else
        out.stim.file = [file_stub '.ns2'];
        [x_stim,y_stim]=extract_all_data_function('Analog 1k', ...
                                              stim_ch,false,out.stim.file);                                    
        out.stim.t = x_stim;
        out.stim.y = y_stim';
          
    end
    % Parse stim onset, offset, and durations
    out.stim.fs = 1000;
    
    stim_threshold = out.stim.y > stim_thresh;
    stim_onset = [0; diff(stim_threshold) == 1];
    stim_offset = [0; diff(stim_threshold) == -1];
    
    out.stim.onset = find(stim_onset);
    out.stim.offset = find(stim_offset);   
    
    % fix starting / stopping in middle of pulse
    if out.stim.offset(1) < out.stim.onset(1)
        out.stim.offset = out.stim.offset(2:end);
    end
    
    if out.stim.offset(end) < out.stim.onset(end)
        out.stim.onset = out.stim.onset(1:end-1);
    end
    out.stim.duration = out.stim.offset - out.stim.onset;
end