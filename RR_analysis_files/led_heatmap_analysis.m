%Analysis of LED and neural data across electrode array
%Adapted from Devon Grigg's code by Ryan Ressmeyer

%% Load Data
close all;
clear;

grey_trace_color=[.7,.7,.7];
mu_char=char(181);

data_folder='/home/ryanress/Nextcloud/Graduate/Yazdan Lab/RR_inhibition_analysis/2021-09-08';

pulse_width = 900;

imp=[7402.3, 8345.4, 16.7, 22.5,...
     12.3, 11.6, 5316.5, 7650.7,...
     5895.1, 7918.1, 8029.4, 7077.0,...
     3006.3, 7961.8, 8307.5, 11391.2,...
     742.1, 1626.1, 33.3, 25.4,...
     18.3, 22.1, 7485.1, 8161.3,...
     7504.5, 7691.7, 23.8, 14.2,...
     22.3, 20.8, 659.2, 20.5];
figure
bar(imp)
title('impedance')

ch_offset=32*4;
channels = [1:16, ch_offset+1:ch_offset+16];
num_channels = length(channels);
laser_ch=10245;

thresh=25;
y_axis_label=[mu_char,'V'];
fs = 1000;

good_ch=[3:6, ch_offset+[3:7, 11:15]];
good_ch_squished=[3:6, (num_channels/2)+[3:7, 11:15]];

trigger_range = [-250, 1150];
trigger_num_samples = trigger_range(2) - trigger_range(1);

fstub = fullfile(data_folder, sprintf('%ums_1s_30p-700mv', pulse_width));


for i = 1:length(channels)
    ch = channels(i);
    [x,y]=extract_all_data_function('LFP',ch,false,[fstub,'.ns2']);
    if ch == channels(1)
        x_lfp = x;
        y_lfp = zeros(length(y), length(channels));
    end
    y_lfp(:,i) = y;
end

[x_analog,y_analog]=extract_all_data_function('Analog 30k',laser_ch,false,[fstub,'.ns5']);
y_led=downsample(y_analog,30);
y_led_threshold = y_led > thresh;
y_led_trigger = [0, diff(y_led_threshold) == 1];
y_led_trigger_idx = find(y_led_trigger);
num_y_led_triggers = length(y_led_trigger_idx);

% range around stimulus onset to plot in samples

y_stim_trig = zeros(trigger_num_samples, num_y_led_triggers, num_channels);

for j = 1:num_y_led_triggers
   start_idx = y_led_trigger_idx(j) + trigger_range(1);
   stop_idx = y_led_trigger_idx(j) + trigger_range(2);
   y_stim_trig(:, j, :) = y_lfp(start_idx:stop_idx-1,:);
end

t_sta = (trigger_range(1):trigger_range(2)-1) / fs;
y_sta = squeeze(mean(y_stim_trig, 2));

%% Plot all 
for i = 1:num_channels
   figure
   plot(t_sta, y_sta(:,i))
end

%% Plot heatmap

pre_range = 1:249;
post_range = 900:1149;

channel_change = mean(y_sta(post_range,:), 1) - mean(y_sta(pre_range,:), 1);
channel_change(imp > 100) = NaN;

figure
mapArray(channel_change);
title('Average \Delta Voltage')
colorbar;

% caxis([min(channel_change), max(channel_change)])

% figure
% plot(imp(imp<100), channel_change(imp<100), 'o')
% grid on;



