%Analysis of LED and neural data
%Adapted from Devon Grigg's code by Ryan Ressmeyer

%% Load Data
close all;
clear;

grey_trace_color=[.7,.7,.7];
mu_char=char(181);

data_folder='/home/ryanress/Nextcloud/Graduate/Yazdan Lab/RR_inhibition_analysis/2021-09-08';

pulse_widths = [900, 800, 700, 600, 500, 400, 300, 200, 100];
num_files = size(pulse_widths,2);

imp=[7402.3, 8345.4, 16.7, 22.5,...
     12.3, 11.6, 5316.5, 7650.7,...
     5895.1, 7918.1, 8029.4, 7077.0,...
     3006.3, 7961.8, 8307.5, 11391.2,...
     742.1, 1626.1, 33.3, 25.4,...
     18.3, 22.1, 7485.1, 8161.3,...
     7504.5, 7691.7, 23.8, 14.2,...
     22.3, 20.8, 659.2, 20.5];
figure
bar(imp)
title('impedance')

example_ch=5;
example_ana_ch=10245;
total_num_channels_on_array=32;
ch_offset=32*4;
thresh=25;
y_axis_label=[mu_char,'V'];
fs = 1000;

good_ch=[3:6, ch_offset+[3:7, 11:15]];
good_ch_squished=[3:6, (total_num_channels_on_array/2)+[3:7, 11:15]];

plot_range = [-250, 1150];
plot_num_samples = plot_range(2) - plot_range(1);
num_pulses = 30;

y_stim_trig = zeros(plot_num_samples, num_pulses, num_files);

for i = 1:num_files
   width = pulse_widths(i);
   fstub = fullfile(data_folder, sprintf('%ums_1s_30p-700mv', width));
   [x,y]=extract_all_data_function('LFP',example_ch,false,[fstub,'.ns2']);
   [x_analog,y_analog]=extract_all_data_function('Analog 30k',example_ana_ch,false,[fstub,'.ns5']);
   y_led=downsample(y_analog,30);
   y_led_threshold = y_led > thresh;
   y_led_trigger = [0, diff(y_led_threshold) == 1];
   y_led_trigger_idx = find(y_led_trigger);
   num_y_led_triggers = size(y_led_trigger_idx, 2);
   % range around stimulus onset to plot in samples
   
   for j = 1:num_y_led_triggers
       start_idx = y_led_trigger_idx(j) + plot_range(1);
       stop_idx = y_led_trigger_idx(j) + plot_range(2);
       y_stim_trig(:, j, i) = y(start_idx:stop_idx-1);
   end
   

end

%% Plot stim triggered averages

pw_to_plot = [900, 700, 500, 300, 100];
num_pw_to_plot = size(pw_to_plot,2);

figure
for i = 1:num_pw_to_plot
    width = pw_to_plot(i);
    width_idx = find(pulse_widths == width);
    y_stacked = y_stim_trig(:,:,width_idx);
    
    ax = subplot_tight(num_pw_to_plot, 1, i, [0.04,0.08]);
    hold on;
    xlim([-.25, 1.15]);
    ylim([-400,400]);

    t_stim = [0, width/fs];
    y_stim = [1,1]*350;
    plot(t_stim, y_stim, 'b');

    t = (plot_range(1):plot_range(2)-1) / fs;
    ste = std(y_stacked');%/sqrt(num_y_led_triggers);

    boundedline(t, mean(y_stacked,2), ste, 'k')
    %plot(t, y_stacked, 'color', grey_trace_color);
    plot(t, mean(y_stacked,2));
    legend(sprintf('%ums', width));
    ylabel(y_axis_label);

    if i == num_pw_to_plot
        xlabel('time (s)')
    end
end

%% Plot sta on same axes

figure
axs = [];
for i = 1:num_files
   width = pulse_widths(i);
   width_idx = find(pulse_widths == width);
   y_stacked = y_stim_trig(:,:,width_idx);

   hold on;
   xlim([-.25, 1.15]);
   ylim([-400,400]);

   t = (plot_range(1):plot_range(2)-1) / fs;
   y = mean(y_stacked,2);
   y = y - mean(y(1:250));
   plot(t, y);

end
l = [];
for w = pulse_widths
    l = [l; sprintf('%ums', w)];
end

legend(l);
ylabel(y_axis_label);
xlabel('Time (s)');
grid();

%% Plot zoomed STA

figure
axs = [];
for i = 1:num_files
   width = pulse_widths(i);
   width_idx = find(pulse_widths == width);
   y_stacked = y_stim_trig(:,:,width_idx);

   hold on;
   xlim([-.005, .01]);

   t = (plot_range(1):plot_range(2)-1) / fs;
   y = mean(y_stacked,2);
   y = y - mean(y(1:250));
   plot(t, y, '-o');

end
l = [];
for w = pulse_widths
    l = [l; sprintf('%ums', w)];
end
l = [l; "Stimulus Onset"];
ylabel(y_axis_label);
xlabel('Time (s)');
grid();

xline(0,'r-',{'Stimulus','Onset'});
legend(l, 'location', 'SouthWest');

%% Bandpass to high gamma

% filter params
n = 2; Wn1 = [1 200]/(fs/2);
ftype = 'bandpass';
% Transfer Function design
[b1,a1] = butter(n,Wn1,ftype);

y_stacked = y_stim_trig(:,:,1);
y_stacked_filt = zeros(size(y_stacked));

for i = 1:num_pulses
    y_stacked_filt = abs(hilbert(filtfilt(b1,a1, y_stacked(:,i))));
end

figure

hold on;
xlim([-.25, 1.15]);

t = (plot_range(1):plot_range(2)-1) / fs;
y = mean(y_stacked_filt,2);
plot(t, y);

%% Plot max deflection for each channel (mean .25s before stimulus onset & offset)


