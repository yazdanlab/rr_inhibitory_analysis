%% Load 2021-06-29 Data
close all;
clear;

grey_trace_color=[.7,.7,.7];
mu_char=char(181);

data_folder='/home/ryanress/Nextcloud/Graduate/Yazdan Lab/RR_inhibition_analysis/2021-06-29-LED-data';

imp_fname = "A7_post_molding_impedance.csv";
imp=load_impedance(fullfile(data_folder, imp_fname));

ch_offset=32*1;
channels = [1:16, ch_offset+1:ch_offset+16];
num_channels = length(channels);
imp = imp(channels);

figure
bar(imp)
title('impedance')

example_ch=5;
laser_ch=10241;
%10241:10270
thresh=25;
y_axis_label=[mu_char,'V'];
fs = 1000;
analog_ds_ratio = 30;

plot_range = [-100, 1100];
plot_num_samples = plot_range(2) - plot_range(1);

fname = 'A7_salinetest_poat_mold_LED_battery';
fstub = fullfile(data_folder, fname);
[x,y]=extract_all_data_function('LFP',example_ch,false,[fstub,'.ns2']);

[x_analog,y_analog]=extract_all_data_function('Analog 30k',laser_ch,false,[fstub,'.ns5']);
y_led=downsample(y_analog,analog_ds_ratio);
figure
plot(x,y_led)
y_led_threshold = y_led > thresh;
y_led_trigger_on = [0, diff(y_led_threshold) == 1];
y_led_trigger_off = [0, diff(y_led_threshold) == -1];

y_led_trigger_on_idx = find(y_led_trigger_on);
y_led_trigger_off_idx = find(y_led_trigger_off);

if length(y_led_trigger_off_idx) < length(y_led_trigger_on_idx)
    y_led_trigger_on_idx = y_led_trigger_on_idx(1:end-1);
end

y_led_trigger_duration = y_led_trigger_off_idx - y_led_trigger_on_idx;

num_y_led_triggers = size(y_led_trigger_on_idx, 2);
y_stim_trig = zeros(plot_num_samples, num_y_led_triggers);
y_illu_trig = zeros(plot_num_samples, num_y_led_triggers);
for j = 1:num_y_led_triggers
   start_idx = y_led_trigger_on_idx(j) + plot_range(1);
   stop_idx = y_led_trigger_on_idx(j) + plot_range(2);
   y_stim_trig(:, j) = y(start_idx:stop_idx-1);
   y_illu_trig(:, j) = y_led(start_idx:stop_idx-1);
end

%% Plot stim triggered average

save_figs = true;
figure
width = 1000;
y_stacked = y_stim_trig(:,y_led_trigger_duration == width);
y_stacked = y_stacked(:,[1:10 12:end]);
y_illu_stacked = y_illu_trig(:,y_led_trigger_duration == width);
y_illu_stacked = y_illu_stacked(:,[1:10 12:end]);

ax = subplot_tight(1, 1, 1, [0.1,0.08]);
xlim([-.25, 1.15]);

t_stim = [0, width/fs];
y_stim = [1,1]*max(max(y_stacked))-50;
plot(t_stim, y_stim, 'b');
hold on 

t = (plot_range(1):plot_range(2)-1) / fs;
y = mean(y_stacked,2);
ste = std(y_stacked');%/sqrt(num_y_led_triggers);

%boundedline(t, y, ste, 'k')
plot(t, y_stacked, 'color', grey_trace_color)
plot(t, y, 'k', 'LineWidth',2)

legend(sprintf('%ums', width));
ylabel(y_axis_label);
xlabel('time (s)')
title(['2021-06-29 Saline STA: Channel ' int2str(example_ch)])
if save_figs
    saveas(gcf, 'figures/2021-06-29_saline_sta','jpeg')
end
% Stimulus average
figure
ax = subplot_tight(1, 1, 1, [0.1,0.08]);
xlim([-.25, 1.15]);

t_stim = [0, width/fs];
y_stim = [1,1]*max(max(y_illu_stacked))-10;
plot(t_stim, y_stim, 'b');

t = (plot_range(1):plot_range(2)-1) / fs;
y = mean(y_illu_stacked,2);
ste = std(y_illu_stacked');%/sqrt(num_y_led_triggers);

boundedline(t, y, ste, 'k')

legend(sprintf('%ums', width));
ylabel(y_axis_label);
xlabel('time (s)')
title(['2021-06-29 Saline Averaged Stimulus: Channel ' int2str(example_ch)])
if save_figs
    saveas(gcf, 'figures/2021-06-29_saline_stim','jpeg')
end

%
% Plot Zoomed STA
%

figure
hold on;
xlim([-.005, .01]);

t = (plot_range(1):plot_range(2)-1) / fs;
y = mean(y_stacked,2);
ste = std(y_stacked');

boundedline(t, y, ste, 'o-k')

ylabel(y_axis_label);
xlabel('Time (s)');
grid();
xline(0,'r-',{'Stimulus','Onset'});
title(['2021-06-29 Saline STA: Channel ' int2str(example_ch)])
if save_figs
    saveas(gcf, 'figures/2021-06-29_saline_sta_zoomed','jpeg')
end

figure

hold on;
xlim([-.005, .01]);

t = (plot_range(1):plot_range(2)-1) / fs;
y = mean(y_illu_stacked,2);

plot(t, y, 'o-')

ylabel(y_axis_label);
xlabel('Time (s)');
grid();
xline(0,'r-',{'Stimulus','Onset'});
title(['2021-06-29 Saline Averaged Stimulus: Channel ' int2str(example_ch)]);
if save_figs
    saveas(gcf, 'figures/2021-06-29_saline_stim_zoomed','jpeg')
end


%% Plot 32 Channels on 4 

close all;
clear;

grey_trace_color=[.7,.7,.7];
mu_char=char(181);

data_folder='/home/ryanress/Nextcloud/Graduate/Yazdan Lab/RR_inhibition_analysis/2021-06-29-LED-data';

imp_fname = "A7_post_molding_impedance.csv";
imp=load_impedance(fullfile(data_folder, imp_fname));

ch_offset=32*4;
channels = [1:16, ch_offset+1:ch_offset+16];
num_channels = length(channels);
imp_channels = [1:16, 33:48];
imp = imp(imp_channels);

figure
bar(imp)
title('impedance')

fname = 'A7_salinetest_poat_mold_LED_battery';
fstub = fullfile(data_folder, fname);

laser_ch=10241;
%10241:10270
thresh=25;
y_axis_label=[mu_char,'V'];
fs = 1000;
analog_ds_ratio = 30;

plot_range = [-100, 1100];
plot_num_samples = plot_range(2) - plot_range(1);

% Get Illumination Traces
[x_analog,y_analog]=extract_all_data_function('Analog 30k',laser_ch,false,[fstub,'.ns5']);
t_led = downsample(x_analog,analog_ds_ratio);
y_led=downsample(y_analog,analog_ds_ratio);

y_led_threshold = y_led > thresh;
y_led_trigger_on = [0, diff(y_led_threshold) == 1];
y_led_trigger_off = [0, diff(y_led_threshold) == -1];

y_led_trigger_on_idx = find(y_led_trigger_on);
y_led_trigger_off_idx = find(y_led_trigger_off);

if length(y_led_trigger_off_idx) == length(y_led_trigger_on_idx) - 1
    y_led_trigger_on_idx = y_led_trigger_on_idx(1:end-1);
end

y_led_trigger_duration = y_led_trigger_off_idx - y_led_trigger_on_idx;

num_y_led_triggers = size(y_led_trigger_on_idx, 2);

figure
plot(t_led,y_led)

% Get Data channels
y_stim_trig = zeros(plot_num_samples, num_y_led_triggers, num_channels);
for i = 1:length(channels)
    ch = channels(i);
    fname = 'A7_salinetest_poat_mold_LED_battery';
    fstub = fullfile(data_folder, fname);
    [x,y]=extract_all_data_function('LFP',ch,false,[fstub,'.ns2']);

    for j = 1:num_y_led_triggers
       start_idx = y_led_trigger_on_idx(j) + plot_range(1);
       stop_idx = y_led_trigger_on_idx(j) + plot_range(2);
       y_stim_trig(:, j, i) = y(start_idx:stop_idx-1);
       
    end
end
%% Plot Stimulus Triggered Averages

t_sta = (plot_range(1):plot_range(2)-1) / fs;

width = 1000;
y_stacked = y_stim_trig(:,y_led_trigger_duration == width,:);
% Exclude 11th stim triggered window 
y_stacked = y_stacked(:,[1:10 12:end],:);
y_sta = squeeze(mean(y_stacked, 2));
figure
for i = 1:4
    ch_start = (i-1)*8+1;
    ch_end = i*8;
    ch_idx = ch_start:ch_end;
    subplot(2,2,i)
    plot(t_sta, y_sta(:,ch_idx))
    title(sprintf('STA for Channels %d - %d', ch_start, ch_end))
    xlabel('t (s)')
    ylabel(y_axis_label)
end
