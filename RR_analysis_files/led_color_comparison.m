%% 
close all;
clear;

root_folder = '/home/ryanress/Nextcloud/Graduate/Yazdan Lab/RR_inhibition_analysis';
data_folder = '2022-01-28';
base_path = fullfile(root_folder, data_folder);

imp_fname = "impedance_2022-01-28.txt";
imp=load_impedance(fullfile(base_path, imp_fname));
imp_channels = [1:16, 33:48];
imp = imp(imp_channels);

ch_offset=32*4;
channels = [1:16, ch_offset+1:ch_offset+16];
num_channels = length(channels);

figure
bar(imp)
title('impedance')

to_save = false;
for ch_idx = find(imp < 200)'
    example_ch = channels(ch_idx);
    stim_ch=10245;
    thresh=25;

    colors = ["red", "blue"];
    voltages = ["300mv", "500mv", "700mv"];

    f_template = "%s-900ms_1s_30p-%s";

    for r = 1:length(voltages)
        for c = 1:length(colors)
            color = colors(c);
            voltage = voltages(r);
            fname = sprintf(f_template, color, voltage);
            full_path = convertStringsToChars(fullfile(base_path,fname));
            key = sprintf("%s_%s", color, voltage);

            data.(key) = loadNSData(full_path, example_ch, ...
                                        false, stim_ch, thresh);
        end 
    end

    figure('Position', [0 0 1000 800])
    abs_exclusion_threshold = Inf; % If any of the windows exceed this value exclude from analysis
    plot_idx = 1;
    for r = 1:length(voltages)
        for c = 1:length(colors)
            color = colors(c);
            voltage = voltages(r);
            key = sprintf("%s_%s", color, voltage);
            trial = data.(key);
            channel_key = sprintf("CH%d", example_ch);
            [t_stacked, y_stacked] = buildTriggeredArray(trial.elec.(channel_key), ...
                                  trial.stim.onset(trial.stim.duration==900),...
                                  -500, 1400, trial.elec.fs);

            exclusion_mask = max(abs(y_stacked)) < abs_exclusion_threshold;
            y_stacked = y_stacked(:,exclusion_mask);

            subplot(length(voltages), length(colors), plot_idx)
            if color == "red"
                linspec=  'r';
            elseif color == "blue"
                linspec = 'b';     
            else
                linspec = 'k';
            end
            plotSTA(t_stacked, y_stacked, linspec);
            
            t_stim = [0, trial.stim.duration(1)/trial.stim.fs];
            yl = ylim;
            y_stim_height = .9;
            y_stim = [1,1]*(yl(1)*(1-y_stim_height) + yl(2)*y_stim_height);
            plot(t_stim, y_stim, 'c-', 'LineWidth', 3);
            
            title_str = sprintf("%s - %s", color, voltage);
            title(title_str)
            plot_idx = plot_idx + 1;
        end 
    end
    suptitle_str = sprintf("2022-01-28 Channel %d - %.1fk\x2126", example_ch, imp(ch_idx));
    suptitle(suptitle_str)
    save_fname = sprintf("2022-01-28_redvblue_wideband_ch%d", example_ch);
    if to_save; saveas(gcf, fullfile('figures/2022-01-28_redvblue_wideband', save_fname), 'jpeg'); end;
end

%% Highpass > 30Hz
close all;
clear;

root_folder = '/home/ryanress/Nextcloud/Graduate/Yazdan Lab/RR_inhibition_analysis';
data_folder = '2022-01-28';
base_path = fullfile(root_folder, data_folder);

imp_fname = "impedance_2022-01-28.txt";
imp=load_impedance(fullfile(base_path, imp_fname));
imp_channels = [1:16, 33:48];
imp = imp(imp_channels);

ch_offset=32*4;
channels = [1:16, ch_offset+1:ch_offset+16];
num_channels = length(channels);

figure
bar(imp)
title('impedance')

to_save = false; 
for ch_idx = 20%find(imp < 200)'
    example_ch = channels(ch_idx);
    stim_ch=10245;
    thresh=25;

    colors = ["red", "blue"];
    voltages = ["300mv", "500mv", "700mv"];

    f_template = "%s-900ms_1s_30p-%s";

    for r = 1:length(voltages)
        for c = 1:length(colors)
            color = colors(c);
            voltage = voltages(r);
            fname = sprintf(f_template, color, voltage);
            full_path = convertStringsToChars(fullfile(base_path,fname));
            key = sprintf("%s_%s", color, voltage);

            data.(key) = loadNSData(full_path, example_ch, ...
                                        false, stim_ch, thresh);
        end 
    end
%8-12
    figure('Position', [0 0 1000 800])
    abs_exclusion_threshold = Inf; % If any of the windows exceed this value exclude from analysis
    plot_idx = 1;
    for r = 1:length(voltages)
        for c = 1:length(colors)
            color = colors(c);
            voltage = voltages(r);
            key = sprintf("%s_%s", color, voltage);
            trial = data.(key);
            channel_key = sprintf("CH%d", example_ch);
            
            ch_y = trial.elec.(channel_key);
            % Filter specifications
            n = 1; Wn1 = [8, 12]/(trial.elec.fs/2);
            ftype = 'bandpass';
            [b,a] = butter(n,Wn1,ftype);
            % Filter the signal
            filtered_ch_y=filtfilt(b,a,ch_y);
            filtered_ch_y = abs(hilbert(filtered_ch_y)).^2;
            
            [t_stacked, y_stacked] = buildTriggeredArray(filtered_ch_y, ...
                                  trial.stim.onset(trial.stim.duration==900),...
                                  -500, 1400, trial.elec.fs);

            exclusion_mask = max(abs(y_stacked)) < abs_exclusion_threshold;
            y_stacked = y_stacked(:,exclusion_mask);

            subplot(length(voltages), length(colors), plot_idx)
            if color == "red"
                linspec=  'r';
            elseif color == "blue"
                linspec = 'b';     
            else
                linspec = 'k';
            end
            plotSTA(t_stacked, y_stacked, linspec);
            
            t_stim = [0, trial.stim.duration(1)/trial.stim.fs];
            yl = ylim;
            y_stim_height = .9;
            y_stim = [1,1]*(yl(1)*(1-y_stim_height) + yl(2)*y_stim_height);
            plot(t_stim, y_stim, 'c-', 'LineWidth', 3);
            
            title_str = sprintf("%s - %s", color, voltage);
            title(title_str)
            plot_idx = plot_idx + 1;
        end 
    end
    suptitle_str = sprintf("2022-01-28 Bandpass 8-30Hz Channel %d - %.1fk\x2126", example_ch, imp(ch_idx));
    suptitle(suptitle_str)
    save_fname = sprintf("2022-01-28_redvblue_wideband_ch%d", example_ch);
    if to_save; saveas(gcf, fullfile('figures/2022-01-28_redvblue_highpass', save_fname), 'jpeg'); end;
end

%% STA Spectrograms

close all;
clear;

root_folder = '/home/ryanress/Nextcloud/Graduate/Yazdan Lab/RR_inhibition_analysis';
data_folder = '2022-01-28';
base_path = fullfile(root_folder, data_folder);

imp_fname = "impedance_2022-01-28.txt";
imp=load_impedance(fullfile(base_path, imp_fname));
imp_channels = [1:16, 33:48];
imp = imp(imp_channels);

ch_offset=32*4;
channels = [1:16, ch_offset+1:ch_offset+16];
num_channels = length(channels);

figure
bar(imp)
title('impedance')

to_save = false; 

example_ch = channels(20);
stim_ch=10245;
thresh=25;

colors = ["red", "blue"];
voltages = ["300mv", "500mv", "700mv"];

f_template = "%s-900ms_1s_30p-%s";

for r = 1:length(voltages)
    for c = 1:length(colors)
        color = colors(c);
        voltage = voltages(r);
        fname = sprintf(f_template, color, voltage);
        full_path = convertStringsToChars(fullfile(base_path,fname));
        key = sprintf("%s_%s", color, voltage);

        data.(key) = loadNSData(full_path, example_ch, ...
                                    false, stim_ch, thresh);
    end 
end

for r = 1:length(voltages)
    for c = 1:length(colors)
        color = colors(c);
        voltage = voltages(r);
        key = sprintf("%s_%s", color, voltage);
        trial = data.(key);
        channel_key = sprintf("CH%d", example_ch);

        ch_y = trial.elec.(channel_key);
        
        offsets = trial.stim.offset(trial.stim.duration == 900);
        off_idx = offsets(end) + 500;
        
        ch_y = ch_y(1:off_idx);
        stim_y = trial.stim.y(1:off_idx);
        trial_t = trial.stim.t(1:off_idx); 

%         figure
%         subplot(3,1,1)
%         T = .5; % .5s
%         W = 2; % 5hz
%         stride_factor = 5;
%         window_params=[T T / stride_factor];
%         params.tapers = [T*W round(2*T*W-1)]; % TW, K (num params
%         params.Fs = trial.elec.fs;
%         params.fpass = [0 200];
%         [S,t,f] = mtspecgramc(ch_y, window_params, params);
%         
%         plot_matrix(S, t, f, 'l')
%         caxis([-10 20]) 
%         
%         subplot(3,1,2)
%         pre = S(t < 2,:);
%         pre_mu = mean(S, 1);
%         pre_std = std(pre); 
%         Z = (S - pre_mu) ./ pre_std;
%         plot_matrix(abs(Z), t, f, 'n')
%         title('abs(Z Score)')
%     
%         caxis([0 5]) 
% 
%         
%         subplot(3,1,3)
%         plot(trial_t, stim_y)
%         
%         suptitle(sprintf("%s %s", color, voltage));
        
        
        win_start = -500;
        win_end = 1400;
        [t_stacked, y_stacked] = buildTriggeredArray(ch_y, ...
                                  trial.stim.onset(trial.stim.duration==900),...
                                  win_start, win_end, trial.elec.fs);
             
        S_avg = 0;
        for k = 1:size(y_stacked, 2)
            T = .1; % .1s
            W = 30; % 30hz
            window_params=[T T/4];
            params.tapers = [T*W round(2*T*W-1)]; % TW, K (num params
            params.Fs = trial.elec.fs;
            params.fpass = [1 200];
            [S,t,f] = mtspecgramc(y_stacked(:,k), window_params, params);
            S_avg = S_avg + S;
        end
        S_avg = S_avg / size(y_stacked, 2);
        t_avg = t + win_start / trial.elec.fs;
        pre = S(t < 2,:);
        pre_mu = mean(S, 1);
        pre_std = std(pre); 
        
        S_avg_pre = S_avg(t_avg < 0,:);
        S_avg_mu = mean(S_avg_pre, 1);
        S_avg_std = std(S_avg_pre);
        Z_avg = (S_avg - S_avg_mu) ./ S_avg_std;
        
        figure
        subplot(2,1,1)
        plot_matrix(S_avg, t_avg, f, 'l');
        caxis([-20 30])
        subplot(2,1,2)
        plot_matrix(Z_avg, t_avg, f, 'n');
        title('Z Score')
        caxis([-10 10])
        suptitle(sprintf("%s %s", color, voltage));
        colormap jet
    end 
end
                         
function plotSTA(t, stacked, LineSpec)
    grey_trace_color=[.7,.7,.7];
    y = mean(stacked,2);
    hold on
    plot(t, stacked, 'color', grey_trace_color);
    plot(t, y, LineSpec, 'LineWidth', 2);
    xlim([min(t), max(t)]);
    
    [m, min_idx] = min(y);
    min_std = std(stacked(min_idx,:));
    [m, max_idx] = max(y);
    max_std = std(stacked(max_idx,:));
    ylim([min(y) - min_std*1.5, max(y) + max_std*1.5])
end

function plotBoundedSTA(t, stacked, LineSpec)
    y = mean(stacked,2);
    ste = std(stacked');
    boundedline(t, y, ste, LineSpec);
end

function [t, stacked] = buildTriggeredArray(signal, triggers, window_start, window_end, fs)
    num_triggers = length(triggers);
    window_width = window_end - window_start+1;
    stacked = zeros(window_width, num_triggers);
    for i = 1:num_triggers
       start_idx = triggers(i) + window_start;
       stop_idx = triggers(i) + window_end;
       stacked(:, i) = signal(start_idx:stop_idx);
    end
    t = (window_start:window_end) / fs;
end

function out = loadNSData(file_stub, channels, stim_30k, stim_ch, stim_thresh)
    
    out.elec.file = [file_stub '.ns2'];
    out.elec.fs = 1000;
    % Load electrode data
    for ch = channels
        [x,y]=extract_all_data_function('LFP',ch,false,out.elec.file);
        out.elec.t = x;
        out.elec.(['CH' int2str(ch)]) = y';
    end
    
    % Load stimulus data
    if stim_30k
        out.stim.file = [file_stub '.ns5'];
        [x_stim,y_stim]=extract_all_data_function('Analog 30k', stim_ch,false,out.stim.file);
        out.stim.t = downsample(x_stim, 30);
        out.stim.y = downsample(y_stim, 30);
    else
        out.stim.file = [file_stub '.ns2'];
        [x_stim,y_stim]=extract_all_data_function('Analog 1k', ...
                                              stim_ch,false,out.stim.file);                                    
        out.stim.t = x_stim;
        out.stim.y = y_stim';
          
    end
    % Parse stim onset, offset, and durations
    out.stim.fs = 1000;
    
    stim_threshold = out.stim.y > stim_thresh;
    stim_onset = [0; diff(stim_threshold) == 1];
    stim_offset = [0; diff(stim_threshold) == -1];
    
    out.stim.onset = find(stim_onset);
    out.stim.offset = find(stim_offset);   
    
    % fix starting / stopping in middle of pulse
    if out.stim.offset(1) < out.stim.onset(1)
        out.stim.offset = out.stim.offset(2:end);
    end
    
    if out.stim.offset(end) < out.stim.onset(end)
        out.stim.onset = out.stim.onset(1:end-1);
    end
    out.stim.duration = out.stim.offset - out.stim.onset;
end
