%% File Names / Locations

clear 
close all

data_folder = '2021-09-01';
imp_fname = "impedance-9-1-21.txt";
data_fname = "900ms_2s_30p_ch14a-110ma";
stim_in_ns5 = true;
%stim_ch = 10245; % For LED data
stim_ch = 10241; % For laser data

plot_title = "2021-09-01 Laser 110ma";

ch_to_plot = "14a";% ch_names(imp < 200);%["16a", "12b"];

% 09-08 LED
% data_folder = '2021-09-08';
% imp_fname = "impedance-9-8-21.txt";
% data_fname = "900ms_1s_30p-700mv";
% stim_in_ns5 = true;
% stim_ch = 10245; % For LED data
% %stim_ch = 10241; % For laser data
% plot_title = "2021-09-08 LED 700mV";
% ch_to_plot = ["6a", "3a", "14b", "5b", "11b"];% ch_names(imp < 200);%["16a", "12b"];

% 02-14 Agar
% data_folder = '2022-02-14-benchside-agar-LED-testing';
% imp_fname = "A5_impedance_2022-02-14_agar.txt";
% data_fname = "A5_RED_agar_3200mV_900ms_30p_1s";
% stim_in_ns5 = false;
% stim_ch = 10245; % For LED data
% %stim_ch = 10241; % For laser data
% plot_title = "2022-02-14 Agar";
% ch_to_plot = ["6a", "3a", "14b", "5b", "11b"];
%% Plot Impedence & Array


root_folder = '/home/ryanress/Nextcloud/Graduate/Yazdan Lab/RR_inhibition_analysis';
%data_folder = '2022-02-14-benchside-agar-LED-testing';
base_path = fullfile(root_folder, data_folder);

num_channels = 32;
ch_idx = 1:num_channels;
ch_names = [""];
for i = 1:16
    ch_names(i) = sprintf("%da", i);
    ch_names(16+i) = sprintf("%db", i);
end
ch_name2ch_idx = containers.Map(ch_names,ch_idx); 

%
% Load impedance data 
%

%imp_fname = "A5_impedance_2022-02-14_agar.txt";
imp_raw=load_impedance(fullfile(base_path, imp_fname));
imp_rows = [1:16, 33:48];
imp = imp_raw(imp_rows);

% X and Y locations for array
resolFactor = 100; % holdover from mapArray.m
ch_x = [4 4 3 2 2 2 1 1 1 1 2 2 3 3 3 4 4 4 5 6 6 7 8 7 7 7 6 6 6 5 5 5]*0.75*resolFactor;
ch_y = [3 1 2 1 5 3 4 2 8 6 9 7 6 4 8 5 7 9 8 9 7 8 5 6 2 4 3 5 1 6 4 2]*0.75*resolFactor;

figure
scatter(ch_x,ch_y, 100, imp, 'filled')
map = [linspace(0,1,200)
       linspace(1,0,200)
       linspace(0,0,200)]';
colormap(map)
colorbar
caxis([min(imp) max(imp)])

for i = ch_idx
    txt = "  "+ch_names(i);
    text(ch_x(i),ch_y(i),txt)
end

title('Impedance Channel Map')

%%
%
% Load NS data
%
ns_ch_offset=32*4;
ns_ch_idx = [1:16, ns_ch_offset+1:ns_ch_offset+16];

%fname = "A5_agar_3200mV_900ms_30p_1s";
%stim_in_ns5 = false;

full_path = convertStringsToChars(fullfile(base_path,data_fname));

for ch_name = ch_to_plot
    plot_ch_idx = ch_name2ch_idx(ch_name);
    plot_ns_ch_idx = ns_ch_idx(plot_ch_idx);
    ch_key = sprintf("CH%d", plot_ns_ch_idx);
    %stim_ch = 10245; % Sometimes 10241
    thresh = 20;
    data = loadNSData(full_path, plot_ns_ch_idx, stim_in_ns5, stim_ch, thresh);
    
%     data.elec.(ch_key) = BPfilter(data.elec.(ch_key),...
%                                 data.elec.fs, 1, 200);
    win_start = -999;
    win_end = 1000;

    % exclude pulse onset if spacing is less than 200ms (i.e. during a pulse
    % train)
    min_trigger_spacing = 200; 
    triggers_mask = [inf; diff(data.stim.onset)] > min_trigger_spacing;
    triggers = data.stim.onset(triggers_mask);

    [t_stacked, y_stacked] = buildTriggeredArray(data.elec.(ch_key), ...
                    triggers, ...
                    win_start, win_end, data.elec.fs);
    
    % ISSUE: Why does demeaning change all freq bands 
    y_stacked = y_stacked - mean(y_stacked, 'all');
    
    y_sta = mean(y_stacked,2);
    % Find pulses during stimulation 
    
    [t_stacked, stim_stacked] = buildTriggeredArray(data.stim.y, ...
                    triggers, ...
                    win_start, win_end, data.elec.fs);
                
    avg_stim = mean(stim_stacked, 2);
    stim_threshold = avg_stim > thresh;
    stim_onset = find([0; diff(stim_threshold) == 1]);
    stim_offset = find([0; diff(stim_threshold) == -1]);
    stim_periods = [t_stacked(stim_onset)' t_stacked(stim_offset)'];
    
    figure
    title(plot_title + " CH" + ch_name)
    
    % Plot STA
    subplot(3,2,1)
    plotSTA(t_stacked, y_stacked, stim_periods, 'r')
    xlabel('time (s)')
    ylabel('uV')
    title('Stimulus Triggered Average') 
    
    % Plot PSDs
    subplot(3,2,2)
    y_sta_pre = y_sta(t_stacked > -.9 & t_stacked <= 0 );
    
    noverlap = 25;
    window_length = 300; % hamming window
    nfft = window_length;
    [pxx_pre,f_pre] = pwelch(y_sta_pre,window_length,noverlap,nfft,data.elec.fs);
    plot(f_pre, 10*log10(pxx_pre))
    hold on
    y_sta_stim = y_sta(t_stacked >= 0 & t_stacked < .9);
    [pxx_post,f_post] = pwelch(y_sta_stim,window_length,noverlap,nfft,data.elec.fs);
    plot(f_post, 10*log10(pxx_post))
    legend('Pre', 'Stim')
    xlabel('Frequency (Hz)')
    ylabel('dB')
    title('Periodograms')
    
    
    % Plot STA Periodogram + Z Score 
    
    T = .1; % s
    W = 20; % hz
    window_params=[T T/4];
    params.tapers = [T*W round(2*T*W-1)]; % TW, K (num params
    params.Fs = data.elec.fs;
    params.fpass = [1 200];
    [S,t,f] = mtspecgramc(y_sta, window_params, params);
    
    t_S = t + win_start / data.elec.fs;

    S_pre = S(t_S < 0,:);
    S_pre_mu = mean(S_pre, 1);
    S_pre_std = std(S_pre);
    Z = (S - S_pre_mu) ./ S_pre_std;

    subplot(3,2,3)
    plot_matrix(S, t_S, f, 'l');
    title('STA Spectrogram')
    %caxis([-20 30])
    subplot(3,2,5)
    plot_matrix(Z, t_S, f, 'n');
    title('STA Z Score')
    caxis([-5 5])
    colormap jet
    
    
    % Plot Spectrogram Average
    S_avg = 0;
    for k = 1:size(y_stacked, 2)
        window_params=[T T/4];
        params.tapers = [T*W round(2*T*W-1)]; % TW, K (num params
        params.Fs = data.elec.fs;
        params.fpass = [1 200];
        [S,t,f] = mtspecgramc(y_stacked(:,k), window_params, params);
        S_avg = S_avg + S;
    end
    S_avg = S_avg / size(y_stacked, 2);
    t_avg = t + win_start / data.elec.fs;

    S_avg_pre = S_avg(t_avg < 0,:);
    S_avg_mu = mean(S_avg_pre, 1);
    S_avg_std = std(S_avg_pre);
    Z_avg = (S_avg - S_avg_mu) ./ S_avg_std;

    subplot(3,2,4)
    plot_matrix(S_avg, t_avg, f, 'l');
    title('Average of Spectrograms')
    %caxis([-20 30])
    subplot(3,2,6)
    plot_matrix(Z_avg, t_avg, f, 'n');
    title('Averaged Z Score')
    caxis([-5 5])
    colormap jet
    
    
    %
    % Spectrogram tests
    %
    
    figure
    
    subplot(2,1,1)
    test_sig = linspace(-49, 59, length(y_sta));
    test_sig = test_sig - mod(test_sig, 25);
    plot(t_stacked, test_sig)
    title("Input Signal")
    
    T = .1; % s
    W = 20; % hz
    window_params=[T T/4];
    params.tapers = [T*W round(2*T*W-1)]; % TW, K (num params
    params.Fs = data.elec.fs;
    params.fpass = [1 200];
    
    [S,t,f] = mtspecgramc(test_sig, window_params, params);
    t_S = t + win_start / data.elec.fs;

    S_pre = S(t_S < 0,:);
    S_pre_mu = mean(S_pre, 1);
    S_pre_std = std(S_pre);
    Z = (S - S_pre_mu) ./ S_pre_std;

    subplot(2,1,2)
    plot_matrix(S, t_S, f, 'l');
    title('Spectrogram')
    %caxis([-20 30])
    colormap jet
    
    %split pre and stim
    figure
    
    T = .1; % s
    W = 20; % hz
    window_params=[T T/4];
    params.tapers = [T*W round(2*T*W-1)]; % TW, K (num params
    params.Fs = data.elec.fs;
    params.fpass = [1 200];
    pre_sig = y_sta(t_stacked < 0 & t_stacked >-.9);
    pre_sig = pre_sig - mean(pre_sig);
    [S,t,f] = mtspecgramc(pre_sig, window_params, params);
    
    t_S = t + win_start / data.elec.fs;
    subplot(2,1,1)
    plot_matrix(S, t_S, f, 'l');
    title('Pre-Stim Spectrogram')
    
    T = .1; % s
    W = 20; % hz
    window_params=[T T/4];
    params.tapers = [T*W round(2*T*W-1)]; % TW, K (num params
    params.Fs = data.elec.fs;
    params.fpass = [1 200];
    post_sig = y_sta(t_stacked > 0 & t_stacked < .9);
    post_sig = post_sig - mean(post_sig);
    [S,t,f] = mtspecgramc(post_sig, window_params, params);
    
    t_S = t + win_start / data.elec.fs;
    subplot(2,1,2)
    plot_matrix(S, t_S, f, 'l');
    title('Stim Spectrogram')
    colormap jet
end

function plotSTA(t, stacked, stim_periods, LineSpec)
    grey_trace_color=[.5,.5,.5, .3];
    y = mean(stacked,2);
    hold on
    xlim([min(t), max(t)]);
    plot(t, stacked, 'color', grey_trace_color);
    
    [m, min_idx] = min(y);
    min_std = std(stacked(min_idx,:));
    [m, max_idx] = max(y);
    max_std = std(stacked(max_idx,:));
    y_min = min(y) - min_std*1.5;
    y_max = max(y) + max_std*1.5; 
    ylim([y_min, y_max])
    
    for i = 1:size(stim_periods, 1)
       x_min = stim_periods(i,1);
       x_max = stim_periods(i,2);
       x_stim = [x_min, x_max, x_max, x_min];
       y_stim = [y_min, y_min, y_max, y_max];
       patch(x_stim, y_stim, 'cyan', 'FaceAlpha', .3, 'LineStyle', 'none');
    end
    
    plot(t, y, LineSpec, 'LineWidth', 2);
    
end
            
function [t, stacked] = buildTriggeredArray(signal, triggers, window_start, window_end, fs)
    num_triggers = length(triggers);
    window_width = window_end - window_start+1;
    stacked = zeros(window_width, num_triggers);
    for i = 1:num_triggers
       start_idx = triggers(i) + window_start;
       stop_idx = triggers(i) + window_end;
       stacked(:, i) = signal(start_idx:stop_idx);
    end
    t = (window_start:window_end) / fs;
end

function out = loadNSData(file_stub, channels, stim_30k, stim_ch, stim_thresh)
    
    out.elec.file = [file_stub '.ns2'];
    out.elec.fs = 1000;
    % Load electrode data
    for ch = channels
        [x,y]=extract_all_data_function('LFP',ch,false,out.elec.file);
        out.elec.t = x;
        out.elec.(['CH' int2str(ch)]) = y';
    end
    
    % Load stimulus data
    if stim_30k
        out.stim.file = [file_stub '.ns5'];
        [x_stim,y_stim]=extract_all_data_function('Analog 30k', stim_ch,false,out.stim.file);
        out.stim.t = downsample(x_stim, 30);
        out.stim.y = downsample(y_stim, 30)';
    else
        out.stim.file = [file_stub '.ns2'];
        [x_stim,y_stim]=extract_all_data_function('Analog 1k', ...
                                              stim_ch,false,out.stim.file);                                    
        out.stim.t = x_stim;
        out.stim.y = y_stim';
          
    end
    % Parse stim onset, offset, and durations
    out.stim.fs = 1000;
    
    stim_threshold = out.stim.y > stim_thresh;
    stim_onset = [0; diff(stim_threshold) == 1];
    stim_offset = [0; diff(stim_threshold) == -1];
    
    out.stim.onset = find(stim_onset);
    out.stim.offset = find(stim_offset);   
    
    % fix starting / stopping in middle of pulse
    if out.stim.offset(1) < out.stim.onset(1)
        out.stim.offset = out.stim.offset(2:end);
    end
    
    if out.stim.offset(end) < out.stim.onset(end)
        out.stim.onset = out.stim.onset(1:end-1);
    end
    out.stim.duration = out.stim.offset - out.stim.onset;
end