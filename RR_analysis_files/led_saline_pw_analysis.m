%Analysis of LED and neural data
%Adapted from Devon Grigg's code by Ryan Ressmeyer

%% Load 2022-01-24 Data
close all;
clear;

grey_trace_color=[.7,.7,.7];
mu_char=char(181);

data_folder='/home/ryanress/Nextcloud/Graduate/Yazdan Lab/RR_inhibition_analysis/2022-01-24';

imp_fname = "A4_impedance_2022-01-24.txt";
imp=load_impedance(fullfile(data_folder, imp_fname));

ch_offset=32*1;
channels = [1:16, ch_offset+1:ch_offset+16];
num_channels = length(channels);
imp = imp(channels);

figure
bar(imp)
title('impedance')

example_ch=15;
laser_ch=10245;

thresh=200;
y_axis_label=[mu_char,'V'];
fs = 1000;
analog_ds_ratio = 1;

plot_range = [-400, 1400];
plot_num_samples = plot_range(2) - plot_range(1);
num_pulses = 30;

y_stim_trig = zeros(plot_num_samples, num_pulses);

color = 'blue';
voltage = '3200mV';
fname = sprintf('A4_fullLED_%s_%s_1s_900ms_30p_saline', color, voltage);
fstub = fullfile(data_folder, fname);
[x,y]=extract_all_data_function('LFP',example_ch,false,[fstub,'.ns2']);
[x_analog,y_analog]=extract_all_data_function('Analog 1k',laser_ch,false,[fstub,'.ns2']);
y_led=downsample(y_analog,analog_ds_ratio);
y_led_threshold = y_led > thresh;
y_led_trigger = [0, diff(y_led_threshold) == 1];
y_led_trigger_idx = find(y_led_trigger);
num_y_led_triggers = size(y_led_trigger_idx, 2);
% range around stimulus onset to plot in samples

for j = 1:num_y_led_triggers
   start_idx = y_led_trigger_idx(j) + plot_range(1);
   stop_idx = y_led_trigger_idx(j) + plot_range(2);
   y_stim_trig(:, j) = y(start_idx:stop_idx-1);
end

%% Plot stim triggered average

figure
width = 900;
y_stacked = y_stim_trig;
ax = subplot_tight(1, 1, 1, [0.1,0.08]);
xlim([-.25, 1.15]);

t_stim = [0, width/fs];
y_stim = [1,1]*max(max(y_stacked))-50;
plot(t_stim, y_stim, 'b');

t = (plot_range(1):plot_range(2)-1) / fs;
y = mean(y_stacked,2);
ste = std(y_stacked');%/sqrt(num_y_led_triggers);

boundedline(t, y, ste, 'k')

legend(sprintf('%ums', width));
ylabel(y_axis_label);
xlabel('time (s)')
title(['Saline STA: ' color ' Full LED - ' voltage ' - Channel ' int2str(example_ch)])
saveas(gcf, sprintf('figures/saline_%s_%s_sta', color, voltage), 'jpeg')

%
% Plot zoomed STA
%
figure
width = 900;
y_stacked = y_stim_trig;

hold on;
xlim([-.005, .01]);

t = (plot_range(1):plot_range(2)-1) / fs;
y = mean(y_stacked,2);
ste = std(y_stacked');%/sqrt(num_y_led_triggers);

boundedline(t, y, ste, 'o-k')

ylabel(y_axis_label);
xlabel('Time (s)');
grid();
xline(0,'r-',{'Stimulus','Onset'});
title(['Saline STA: ' color ' Full LED - ' voltage ' - Channel ' int2str(example_ch)])
saveas(gcf, sprintf('figures/saline_%s_%s_sta_zoomed', color, voltage), 'jpeg')
