## Ryan Ressmeyer's Inhibitory Data Analysis for the Yazdan Lab

### Relevant files & folders
- analysis_template.m: A template file containing a compressed version of the majorioty of analyses I ran. 
- BPfilter.m: A bandpass filtering function provided by Azadeh Yazdan.
- SignalPower.m: A function to calculate average signal power provided by Azadeh Yazdan.
- load_impedance.m: A function which parses an impedance.txt file.
- extract_all_data_function.m: A function to load .ns2/5 files provided by Devon Griggs.
- Tools: Directory containing all matlab tools I used. Add path and subpath to run scripts.
- figures: Directory containing various figures I generated during my rotation.
- RR_analysis_files: Working scripts that I used at various points during my rotation. Most of their functionality has been condensed into analysis_template.m, but feel free to take a look if you're interested.